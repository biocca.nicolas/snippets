# Python
Welcome to the Python snippets directory!
This directory contains a collection of Python code snipptes that cover several concepts and can be useful for reuse when needed. 
Some topics I'd to cover include libraries such as Numpy, Pandas, Matplotlib, Bokeh. 
In addition, you can find Python concepts such as Packaging, OOP, decorators, envs, and so on.

## Structure

- **grd_reader**: reads shyfem grd `grd` formatted mesh. Init Mesh class development. 
  - [Link to grd_reader](grd_reader/)

- **graph_search_visualization**: visualize connected graph traverse by means of BFS and DFS algorithms.
  - [Link to graph_search_visualization](graph_search_visualization/)
