import os, sys
from mesh_utils import Mesh



def main(input_filename):

    print('info: creating Mesh instance')
    mesh = Mesh()
    mesh.read_grd_mesh(input_filename)

    print(mesh)

if __name__ == '__main__':

    #filename = sys.argv[1]
    filename = 'test/mesh.grd'
    if os.path.isfile(filename):
       main(filename)
    else:
        sys.exit(f'error: {os.path.basename(filename)} does not exist')