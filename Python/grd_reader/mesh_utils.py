import os
import sys
import numpy as np

class Mesh():
    '''
        Specific class for Shyfem/Muse models.
    '''
    def __init__(self):
        
        self.dimension = 2
        self.nb_nodes = 0
        self.nb_elems = 0

        self.coords = None
        self.je     = None
        self.bathymetry = None

        self.nodes_id = None
    
    def __repr__(self):
        return (
            f'Mesh statistics:\n'
            f'\tnodes = {self.nb_nodes}\n'
            f'\telems = {self.nb_elems}'
        )

    #
    # Getters
    # 
    
    def get_nb_nodes(self):
        return self.nb_nodes
    
    def get_nb_elems(self):
        return self.nb_elems
    
    def get_coordinates(self):
        return self.coords
    
    def get_connectivity(self):
        return self.je
    
    def get_bathymetry(self):
        return self.bathymetry
    
    def get_node_numbering(self):
        return self.nodes_id
    
    def get_element_numbering(self):
        return self.elems_id
    
    # ----------------------------------------- 
    # -----------------------------------------
    
    def read_grd_mesh(self, filename):
        '''
            reads grd mesh
        '''

        if not os.path.isfile(filename):
            print(f'info: {filename} does not exist')
            return

        nodes_id = list()
        elems_id = list()
        bathy    = list()
        coords   = list()
        je       = list()


        with open(filename,'r') as f:
            for line in f:
                records = line.split()

                if not records:
                    continue

                if records[0] == '1': # node tag
                    self.nb_nodes += 1

                    nodes_id.append(int(records[1]))
                    
                    coords.append(float(records[3]))
                    coords.append(float(records[4]))

                    if len(records) == 6:                  # optional field -> bathymetry
                        bathy.append(float(records[5]))
                
                if records[0] == '2':  # element tag
                    self.nb_elems += 1

                    elems_id.append(int(records[1]))

                    je.append(int(records[4]))
                    je.append(int(records[5]))
                    je.append(int(records[6]))

                    if len(records) == 8:                    # optional field -> bathymetry
                        bathy.append(float(records[7]))
        
        
        # cast to Numpy arrays
        self.coords = np.array(coords).reshape((-1,self.dimension))
        self.je = np.array(je).reshape((-1,3))
        self.bathymetry = np.array(bathy)
        self.nodes_id = np.array(nodes_id)
        self.elems_id = np.array(elems_id)
