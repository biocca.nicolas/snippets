import networkx as nx
from graph_algorithms import order_bfs, order_dfs
from graph_visualization import visualize_search

from unit_tests import G_01, pos_01, G_02, pos_02

def test_01():

    G = G_01
    pos = pos_01

    order_bfs_result = order_bfs(G, start_node='A')
    visualize_search(order_bfs_result, 'BFS Visualization', G, pos)

def test_02():

    G = G_01
    pos = pos_01

    order_dfs_result = order_dfs(G, start_node='A')
    visualize_search(order_dfs_result, 'DFS Visualization', G, pos)

def test_03():

    G = G_02
    pos = pos_02

    order_bfs_result = order_bfs(G, start_node=0)
    visualize_search(order_bfs_result, 'BFS Visualization', G, pos,savefig=True)

def test_04():
    
    G = G_02
    pos = pos_02
    
    order_dfs_result = order_dfs(G, start_node=0)
    visualize_search(order_dfs_result, 'DFS Visualization', G, pos,savefig=True)

if __name__ == '__main__':

    #test_01()
    #test_02()

    test_03()
    test_04()

