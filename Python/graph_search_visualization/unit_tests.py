#
# Compute 
# 

import networkx as nx

def generate_connected_random_graph(n_nodes:int,n_edges:int):
    
    while True:
        G = nx.gnm_random_graph(n_nodes,n_edges)
        if nx.is_connected(G):
            return G

G_01 = nx.Graph()
G_01.add_edges_from([
    ('A','B'),
    ('A','C'),
    ('B','D'),
    ('B','E'),
    ('C','F'),
    ('C','G')
])
pos_01 = nx.spring_layout(G_01)

G_02 = generate_connected_random_graph(n_nodes=10, n_edges=15)
pos_02 = nx.spring_layout(G_02)