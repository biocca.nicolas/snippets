#
# graph visualization
# 

import networkx as nx
import matplotlib.pyplot as plt
import time

def visualize_search(order, title, G, pos,savefig=False):

    plt.figure()
    plt.title(title)
    for i, node in enumerate(order,start=1):
        plt.clf()
        plt.title(title)
        nx.draw(G,pos,with_labels=True,node_color=['r' if n == node else 'g' for n in G.nodes ])
        plt.draw()
        plt.pause(0.5)  # secs
        
        # save plot
        if savefig:
            filename = f"{title.lower().replace(' ','_')}_{i:02d}.png"
            plt.savefig(filename,dpi=200)
        
    plt.show()
    time.sleep(0.5)