
#
# Graph algorithm module
# 

import queue

def order_bfs(graph, start_node):

    visited = set()     # unique elements
    q = queue.Queue()   # FIFO queue
    q.put(start_node)
    order = list()

    while not q.empty():
        vertex = q.get()
        if vertex not in visited:
            order.append(vertex)
            visited.add(vertex)
            for node in graph[vertex]:
                if node not in visited:
                    q.put(node)
    
    return order


def order_dfs(graph, start_node, visited=None):
    
    if visited is None:   # first iteration
        visited = set()

    order = list()

    if start_node not in visited:
        order.append(start_node)
        visited.add(start_node)
        for node in graph[start_node]:
            if node not in visited:
                order.extend(order_dfs(graph,node,visited))
    
    return order