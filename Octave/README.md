# GNU Octave

This repository contains useful scripts and function for everyday tasks.

## Installation

To provide a user with system-wide access to the scripts, they should be located reachable by the OCTAVE PATH. 

Add the following line to your `.octaverc`:

```octave
addpath('~/.octaverc');
```

You can then store all your scripts in the `~/octave` directory.


## Functions 

- clipboard

