% CLIPBOARD - Copy data to the system clipboard based on the operating system.
%
%   clipboard(data) copies the provided data to the system clipboard,
%   automatically selecting the appropriate method for the operating system:
%   - On Windows, it uses 'clip.exe'.
%   - On macOS, it uses 'pbcopy'.
%   - On Linux, it uses 'xclip'.
%
%   This makes the function compatible with multiple operating systems.
%
%   In addition, It can hadle arrays, which is its main goal.
%
%   Written by Nicolás Biocca

function clipboard(data)

  % Handle numeric data. 
  is_array = false;
  if ~ischar(data)
      data = mat2str(data);
	  is_array = true;
  end

  data = regexprep(data, ';','\n');
  if is_array 
	data = data(2:end-1);    % remove square brackets [] from output 
  end

  f = tempname;
  h = fopen(f, 'w');
  fprintf(h, data);
  fclose(h);

  % Automatically copy data to the system clipboard based on the operating system.
  if ispc          % Windows system
    system(['clip.exe < ' f]);
  elseif ismac     % macOS system (Note: macOS uses pbcopy)
    system(['cat ' f ' | pbcopy']);
  else 			   % Assume Linux system (using xclip)
    system(['cat ' f ' | xclip -selection clipboard']);
  end

  delete(f);
end
