#include <stdio.h>
#include <stdlib.h>
#include "join_files.h"

int main(int argc, char* argv[]) {

    if (argc != 4) {
        printf("Usage: %s <file1> <file2> <output>\n", argv[0]);
        return 1;
    }


    const char* file1  = argv[1]; 
    const char* file2  = argv[2];  
    const char* output = argv[3];  

    printf("info: starting merge files test\n");

    join_uv_files(file1,file2,output);

    printf("info: files merged successfully\n");
    
    return 0;
}
