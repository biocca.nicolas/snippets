#include <stdio.h>
#include <stdlib.h>

void join_uv_files(const char* file1, const char* file2, const char* outputFile){
    //
    // join two files file1 and file2 onto outputFile file using separatorString as 
    // separator string between each other
    //
    FILE* fptr1;   // u  velocity file  --> modify header 
    FILE* fptr2;   // v  velocity file  --> skip header (3 lines) before copy it
    FILE* fout;    // uv velocity file

    // open 1st file input file in read mode
    fptr1 = fopen(file1,"r");
    if (fptr1 == NULL) {
        printf("error: %s does not exist.\n",file1);
        return;
    }

    // open 2nd file input file in read mode
    fptr2 = fopen(file2,"r");
    if (fptr2 == NULL) {
        printf("error: %s does not exist.\n",file2);
        return;
    }

    // Open output file in write mode
    fout = fopen(outputFile, "w");
    if (fout == NULL) {
        printf("error: can not create %s file", outputFile);
        fclose(fptr1);
        fclose(fptr2);
        return;
    }


    // Read the first line from file1 and modify the 6th value
    int val1, val2, val3, val4, val5, val6, val7;
    if (fscanf(fptr1, "%d %d %d %d %d %d %d", &val1, &val2, &val3, &val4, &val5, &val6, &val7) != 7) {
        printf("error: file formatting. 7 values were expected on 1st line");
        fclose(fptr1);
        fclose(fptr2);
        fclose(fout);
        return;
    }
    val6 = 2;
    // Write the modified first line to the output file
    fprintf(fout, "%d %d %d %d %d %d %d", val1, val2, val3, val4, val5, val6, val7);

    int ch;
    
    // Copy rest of file1 to output file
    while ((ch = fgetc(fptr1)) != EOF) {
        fputc(ch, fout);
    }

    fprintf(fout, "\n");

    // Skip the first 3 lines on file2
    for (int i = 0; i < 3; i++) {
        while ((ch = fgetc(fptr2)) != EOF) {
            if (ch == '\n') {
                break;
            }
        }
    }

    // Copy contents of file2 to output file
    while ((ch = fgetc(fptr2)) != EOF) {
        fputc(ch, fout);
    }

    // Close files
    fclose(fptr1);
    fclose(fptr2);
    fclose(fout);

    return;
}



