// Function Pointers
// Ability to store a function address of a specfic function signature
// 
//   - Encapsule (or hold) the behaviour of a function in a pointer 




#include <stdio.h>

int add(int a, int b){
    return a+b;
}

int multiply(int a, int b){
    return a*b;
}

int main(){

    printf("&add = %p\n",&add);

    // Create a function pointer
    int (*pfn)(int,int) = &add;
    printf("pfn  = %p\n",pfn);

    printf("2+7 = %d\n",pfn(2,7));

    pfn = &multiply;
    printf("2*7 = %d\n",pfn(2,7));

    return 0;
}
