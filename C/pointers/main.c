#include <stdlib.h>
#include <stdio.h>

void swap(int *n1, int *n2);

int main(int argc, char* argv[]){
    
    int a = 10;
    int *p = &a;   // &a denotes the address of a


    printf("Do not misundestood int *p (int pointer type) and\n");
    printf("*p (dereference of pointer p) sytax\n\n");


    printf("p  = %d\n",p);    // print the adress p is actually storing (&a)
    printf("&a = %d\n",&a);   // address of a 
    printf("a  = %d\n",a);    // value stored in a
    printf("*p = %d\n",*p);   // dereference p pointer. * dereference operator.


    printf("\nWe can modify the value p is pointing out (var a)\n");
    *p = 20;
    printf("*p = %d\n",*p);
    printf("a  = %d\n",a);


   
    printf("\nWorking with arrays\n");
    printf("int arr[] = {1,2,3};\n");
    int arr[] = {1,2,3};
    printf("arr = %d\n",arr);               // memory address. arr is pointing to the 1st item memory
    printf("*arr = %d\n",*arr);             // gives the value of the 1st item in arr
    printf("arr[1] = %d\n",arr[1]);         // value at arr[1]
    printf("*(arr+1) = %d\n",*(arr+1));     // derefence the value at memory address arr[1] 
    printf("(arr+1) = %d\n",(arr+1));       // memory address of arr[1]
    printf("&arr[1] = %d\n",&arr[1]);       // memory address of arr[1]


    //
    // Pointer arithmetic
    //
    printf("\nPointer arithmetic\n");
    printf("\tIncrementing a pointer\n");
    p = arr;
    for (int i = 0; i < sizeof(arr)/sizeof(int); i++){
        printf("p = %d  *p = %d\n",p,*p);
        printf("p++\n");
        p++;
    }

    printf("\n\tPointer Comparisson\n");
    p = arr;
    while ( p <= &arr[2] ) {
        printf("p = %d  *p = %d\n",p,*p);
        p++;
    }

    printf("\nPointer to Pointer - Double pointers\n");
    int **pp;
    p  = &arr[0];    // it's enough just arr but to let put it clear that any pointer stores a memory address
    pp = &p;         // p stores a memory and also &p is a memory address per se (of course!)
    printf("arr[0] = %d\n",arr[0]);
    printf("    *p = %d\n",*p);
    printf("  **pp = %d\n",**pp);

    **pp *= 2;  
    printf("**p *= 2;\n");
    printf("arr[0] = %d\n",arr[0]);
    printf("    *p = %d\n",*p);
    printf("  **pp = %d\n",**pp);


    //
    // Passing addresses to functions
    //
    printf("\nPassing memory addresses to function\n");
    int num1 = 1;
    int num2 = 5;
    
    printf("num1 = %d\n", num1);
    printf("num2 = %d\n", num2);
    swap(&num1,&num2);
    printf("swap(&num1,&num2);\n");
    printf("num1 = %d\n", num1);
    printf("num2 = %d\n", num2);

    
    for (int i = 0; i < sizeof(arr)/sizeof(int); i++){
        printf("arr[%d] = %d\n",i,arr[i]);
    }
    swap(&arr[0],&arr[2]);
    printf("swap(arr[0],arr[2]);\n");
    for (int i = 0; i < sizeof(arr)/sizeof(int); i++){
        printf("arr[%d] = %d\n",i,arr[i]);
    }

    //
    // Pointers to structures
    //
    printf("\n\n### Pointers to Structures###\n");
    typedef struct person{
        int age;
        int* array;
    }person;

    person p1;
    p1.age = 25;
    p1.array = malloc(sizeof(int)*5);


    printf("p1.age = %d\n",p1.age);
    for (int i=1;i<5;i++){
        p1.array[i] = i*i;
    }

    for (int i=0;i<5;i++){
        printf("p1.array[%d] = %d\n",i,p1.array[i]);
    }


    person* p_p1;
    p_p1 = &p1;
    printf("  p_p1->array[0] = %d located at %p\n",p_p1->array[0],&(p_p1->array[0]));
    printf("(*p_p1).array[0] = %d located at %p\n",(*p_p1).array[0], &((*p_p1).array[0]));

    person** pp_p1;
    pp_p1 = &p_p1;
    printf("located at %p\n",&((**pp_p1).array[0]));

    

  return 0; 
}


void swap(int *n1, int *n2){

    int temp; 

    temp = *n1;

    *n1 = *n2;
    *n2 = temp;
}
