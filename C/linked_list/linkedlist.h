#ifndef LINKED_LIST_H
#define LINKED_LIST_H
// Individual node in the chain
typedef struct node{
    int data;
    struct node* next;
}node_t;

// Linked list data structure, which always hold the 1st node
// in our chain
typedef struct linkedlist{
    node_t* head;
}linkedlist_t;

// malloc a new linked list. Return a pointer to it 
linkedlist_t* CreateLinkedListOfFiveItems();

// Create an empty linked list
linkedlist_t* CreateLinkedList();

// Walk through linked list and print all nodes
void PrintLinkedList(linkedlist_t* list);

// Walk through linked list and delete all nodes
void FreeLinkedList(linkedlist_t*);

// Create a new node_t and add to the end of linkedlist_t
void AppendToLinkedList(linkedlist_t* list, int data);

// Create a new node_t and add to the beginning of linkedlist_t
void PrependToLinkedList(linkedlist_t* list, int data);

// Create a new node_t and add to a specific location based on index
void InsertAtIndex(linkedlist_t* list, int index, int data);
#endif
