#include "linkedlist.h"
#include <stdio.h>      // printf
#include <stdlib.h>     // malloc/free and NULL

// malloc a new linked list. Return a pointer to it 
linkedlist_t* CreateLinkedListOfFiveItems(){
    
    linkedlist_t *newlist = CreateLinkedList();

    node_t* newnode1 = (node_t*)malloc(sizeof(node_t));
    newnode1->data = 1; 

    node_t* newnode2 = (node_t*)malloc(sizeof(node_t));
    newnode2->data = 2;

    node_t* newnode3 = (node_t*)malloc(sizeof(node_t));
    newnode3->data = 3; 

    node_t* newnode4 = (node_t*)malloc(sizeof(node_t));
    newnode4->data = 4; 

    node_t* newnode5 = (node_t*)malloc(sizeof(node_t));
    newnode5->data = 5;
    
    // Point the list->head to the 1st node
    newlist->head = newnode1;


    newnode1->next = newnode2;
    newnode2->next = newnode3;
    newnode3->next = newnode4;
    newnode4->next = newnode5;
    newnode5->next = NULL;

    return newlist; 
}

// Create an empty linked list
linkedlist_t* CreateLinkedList(){
    linkedlist_t* newList = (linkedlist_t*)malloc(sizeof(linkedlist_t)*1);

    newList->head = NULL;
    
    return newList;
}

// Walk through linked list and print all nodes
void PrintLinkedList(linkedlist_t* list){

    node_t* iter = list->head;
    while(iter != NULL){
        printf("data %d\n",iter->data);
        iter = iter->next;
    }
}

// Walk through linked list and delete all nodes
void FreeLinkedList(linkedlist_t* list){

    if (list==NULL){
        return;
    }

    node_t* current = list->head;  // points to the current node
    if (current==NULL){
        free(list);
        return;
    }
    node_t* next = current->next; // list->head->next;
    while (current !=NULL) {
        free(current);        // free current node
        current = next;       // set current to next node
        // Ensure that current is not the end
        // then access (dereference) current-> next
        if (current != NULL) {
            next = current->next; // set next to the next one node
        }
    }
    // Last step, free linkedlist_t struct
    free(list);
}

// Create a new node_t and add to the end of linkedlist_t
void AppendToLinkedList(linkedlist_t* list, int data){

    if (list->head == NULL) { // linked list is empty
        node_t* newnode = (node_t*)malloc(sizeof(node_t));
        newnode->data = data;
        newnode->next = NULL;

        // set the head of out list to our newly allocated node
        list->head = newnode; 
    }else{
        node_t* iter = list->head;

        while (iter->next != NULL) {
            iter = iter->next;
        }
        // Once we find the end of the singly linked list (sll) 
        // Create a new node here
        node_t* newnode = (node_t*)malloc(sizeof(node_t));
        newnode->data = data;
        newnode->next = NULL;

        iter->next = newnode;
    }

}

// Create a new node_t and add to the beginning of linkedlist_t
void PrependToLinkedList(linkedlist_t* list, int data){

    node_t* newnode = (node_t*)malloc(sizeof(node_t));
    newnode->data = data;
    // Set the new node's next pointer to the current head
    newnode->next = list->head;
    // Update the head pointer to point to the new node
    list->head = newnode;
}

void InsertAtIndex(linkedlist_t* list, int index, int data){

    //Check for valid index
    if (index<0){
        printf("info: invalid index (%d)\n",index);
        return;
    }

    node_t* newnode = (node_t*)malloc(sizeof(node_t));
    newnode->data = data;

    // if inserting at index 0, update head pointer in linkedlist_t
    if (index==0){
       newnode->next = list->head;
       list->head = newnode;
       return;
    }

    // otherwise, traverse the linked list to find the node before the desired index
    node_t* current = list->head;
    int currentIndex = 0;
    while (current != NULL && currentIndex < index - 1){
        current = current->next;
        currentIndex++;
    }

    // check if index is out of bounds
    if (current == NULL){
        printf("info: index (%d) out of bounds\n",index);
        free(newnode);
        return;
    }

    // Insert new node between current and current->next
    newnode->next = current->next;
    current->next = newnode;
        
}




