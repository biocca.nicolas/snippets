#include <stdio.h>
#include "linkedlist.h"

void unitTest1(){

    linkedlist_t *newlist = CreateLinkedListOfFiveItems();
    AppendToLinkedList(newlist,6);
    AppendToLinkedList(newlist,7);
    AppendToLinkedList(newlist,8);

    PrintLinkedList(newlist);

    FreeLinkedList(newlist);
}

void unitTest2(){
    linkedlist_t *newlist = CreateLinkedList();
    FreeLinkedList(newlist);
}


void unitTest3(){
    linkedlist_t *newlist = CreateLinkedList();
    PrependToLinkedList(newlist,1);
    PrependToLinkedList(newlist,2);
    PrependToLinkedList(newlist,3);
    PrintLinkedList(newlist);
    FreeLinkedList(newlist);
}

void unitTest4(){
    linkedlist_t *newlist = CreateLinkedList();
    AppendToLinkedList(newlist,1);
    AppendToLinkedList(newlist,2);
    PrependToLinkedList(newlist,0);
    PrintLinkedList(newlist);
    FreeLinkedList(newlist);
}

void unitTest5(){
    linkedlist_t *newlist = CreateLinkedListOfFiveItems();
    InsertAtIndex(newlist,6,10); 
    PrintLinkedList(newlist);
    FreeLinkedList(newlist);
}

void unitTest6(){
    linkedlist_t *newlist = CreateLinkedListOfFiveItems();
    InsertAtIndex(newlist,0,10); 
    PrintLinkedList(newlist);
    FreeLinkedList(newlist);
}
void unitTest7(){
    linkedlist_t *newlist = CreateLinkedListOfFiveItems();
    InsertAtIndex(newlist,5,10); 
    PrintLinkedList(newlist);
    FreeLinkedList(newlist);
}
int main(){

    unitTest1();
    unitTest2();
    unitTest3();
    unitTest4();
    unitTest5();
    unitTest6();
    unitTest7();

    return 0;
}
