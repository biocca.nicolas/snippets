# Singly Linked List Implementation (Work in Progress)

This is a C code snippet for a singly linked list implementation. Please note that this implementation is a work in progress (WiP). While some basic functionality has been implemented, there is still work to be done to complete the full feature set.

## Features Implemented (So Far)

1. **Data Structures**: 
   - `struct node`: Represents a node in the linked list.
   - `struct linkedlist`: Represents the linked list itself.

2. **Allocation and Appending Data**:
   - Functions to allocate and append data to the linked list are implemented.

3. **Prepending Data**:
   - Function to prepend data to the linked list is implemented.

4. **Inserting at Index**::
   - Function to insert data at a given index location. 

5. **Free Linked List Memory Function**:
   - Function to deallocate linked list memory.

6. **Print Function**:
   - An implementation for a print function to display the linked list has been completed.

7. **Unit Tests**:
   - UnitTest1: test append method.
   - UnitTest2: test free an empty list. 
   - UnitTest3: test prepend method.
   - UnitTest4: test append and prepend methods. 
   - UnitTest5-7: test insert at index method. 

 
