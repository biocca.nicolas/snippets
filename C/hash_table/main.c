//
// References: 
//      - https://www.youtube.com/watch?v=wg8hZxMRwcw
//      - https://github.com/engineer-man/youtube/blob/master/077/hashtable.c
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hashtable.h"



int main(){
    

    // testing hash function
    unsigned int slot;

    printf("# Testing hash function:\n");
    printf("hash(\"Nicolas\") = %d\n",hash("Nicolas"));
    printf("hash(\"Nicolás\") = %d\n",hash("Nicolás"));
    printf("hash(\"Hi\")      = %d\n",hash("Hi"));
    printf("hash(\"bye\")     = %d\n",hash("bye"));
    printf("hash(\"Welcome\") = %d\n",hash("Welcome"));

    
    printf("\nCreating hash table\n");
    ht_t *ht = ht_create();

    printf("\nFilling hash table\n");
    ht_set(ht, "name1", "em");
    ht_set(ht, "name2", "russian");
    ht_set(ht, "name3", "pizza");
    ht_set(ht, "name4", "doge");
    ht_set(ht, "name5", "pyro");
    ht_set(ht, "name6", "joost");
    ht_set(ht, "name7", "kalix");

    printf("\nGetting values from hash table\n");
    printf("ht_get(ht, \"name1\") = %s\n",ht_get(ht, "name1")); 
    printf("ht_get(ht, \"name2\") = %s\n",ht_get(ht, "name2")); 
    printf("ht_get(ht, \"name3\") = %s\n",ht_get(ht, "name3")); 
    printf("ht_get(ht, \"name4\") = %s\n",ht_get(ht, "name4")); 
    printf("ht_get(ht, \"name5\") = %s\n",ht_get(ht, "name5")); 
    printf("ht_get(ht, \"name6\") = %s\n",ht_get(ht, "name6")); 
    printf("ht_get(ht, \"name7\") = %s\n",ht_get(ht, "name7")); 


    printf("\nTesting ht_dump procedure\n");
    ht_dump(ht);

    printf("\nTesting ht_del procedure\n");
    ht_del(ht,"name1");

    printf("Printing out hash table after key deletion\n");
    ht_dump(ht);

    return 0;
}
