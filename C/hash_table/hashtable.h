#ifndef HASH_TABLE_H
#define HASH_TABLE_H

#define TABLE_SIZE 10

typedef struct entry_t {
    char *key;              // pointer to character array
    char *value;            // pointer to character array
    struct entry_t *next;   // pointer to another entry_t structure. Used to handle colision in the hast table.
} entry_t;

typedef struct {
    entry_t **entries;     // pointer to entry_t pointer. It's used to store an array of pointer to entry_t structures. 
} ht_t;

// hash function
unsigned int hash(const char *key);

// allocate and set entry_t -> key, value 
entry_t *ht_pair(const char *key, const char *value); 

// allocate and init hash table
ht_t *ht_create();

// set value in hash table
void ht_set(ht_t *hashtable, const char *key, const char *value);

// get value from hast table
char *ht_get(ht_t *hashtable, const char *key);

// delete key from hast table
void ht_del(ht_t *hashtable, const char *key);

// dump hash table
void ht_dump(ht_t *hashtable);

#endif
