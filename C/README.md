# C

Welcome to the C snippets directory! This directory contains a collection of C code snippets, each organized in its own subdirectory. These snippets cover various C programming concepts and can be helpful for learning, reference, and reusability as well.

## Structure

- **shyfem_grd_reader**: reads a Shyfem `grd` formatted mesh.
  - [Link to shyfem grd reader](shyfem_grd_reader/)

- **join_uv_files**: join u and v velocity files into uv file. Specific purpose routine for `waluigi`. 
  - [Link to join_uv_files](join_uv_files/)

- **linked_list**: Introduction to Singly Linked Lists (SLL) 
  - [Link to pointers](linked_list/)

- **pointer**: brief introduction to pointers, double pointers, common syntax, etc.
  - [Link to pointers](pointers/)

- **function_pointer**: introduction to function pointer.
  - [Link to function_pointers](function_pointers/)

- **binary_tree**: introduction to binary tree. 
  - [Link to binary_tree](binary_tree/)

- **hash_table**: introduction to hash tables. **(WiP)** 
  - [Link to hash_table](hash_table/)

- **stack**: Introduction to stack data structure. **(WiP)**
  - [Link to stack](stack/)

- **queue**: Introduction to queue data structure.
  - [Link to queue](queue/)
