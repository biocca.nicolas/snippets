#include <stdio.h>
#include <limits.h>
#include <stdbool.h>

//
// Reference: 
//      - Jacob Sorber  | https://www.youtube.com/watch?v=A4sRhuGkRb0
//      - GeeksforGeeks | https://www.geeksforgeeks.org/stack-data-structure/?ref=shm
//

// TODO: - Isolate implementation for general purposes, including header file. 
//       - Implement unit tests
//       - Implement generic container (i.e., data structure)
//       - Implement OOP version of stack
//       - Add makefile

#define STACK_LENGTH 5
#define EMPTY -1
#define STACK_EMPTY INT_MIN

int mystack[STACK_LENGTH];
int top = EMPTY;


bool push(int value){

    if (top >= STACK_LENGTH-1) return false;

    top++;
    mystack[top] = value;

    return true;
}

int pop() {

    if (top == EMPTY) return STACK_EMPTY;

    int result = mystack[top];
    top--;
    return result; 
}

int main() {

    push(10);
    push(20);
    push(30);
    push(40);


    int t;
    while ( (t = pop()) != STACK_EMPTY ) {
        printf("t = %d\n",t);
    }
    return 0;
}
