//
// Contains functions to read a grd SHYFEM mesh.
//
// Functions:
//  - get_dims: get number of nodes and elements.
//  - read_mesh: get nodal ids, element ids, mesh incidence and nodal coordinates.
//
// TODO: Sort mesh if the nodal ids are not in sequence. 
//

#include <stdio.h>
#include <string.h>
#include "grd_reader.h"


int get_dims(const char* filename, int* nb_nodes, int* nb_elems){

    FILE *file;
    char line[MAX_LINE_LENGTH];

    // Open the file in read mode
    file = fopen(filename, "r");
    if (file == NULL) {
        printf("Failed to open the file.\n");
        return 1; // Return an error code indicating failure
    }

    *nb_nodes = 0;
    *nb_elems = 0;

    // Read each line and check if it starts with "1" or "2"
    while (fgets(line, sizeof(line), file) != NULL) {
        // Use sscanf to extract the first field as an integer
        int value;
        if (sscanf(line, "%d", &value) == 1) {
            if (value == 1) {
                (*nb_nodes)++;
            } else if (value == 2) {
                (*nb_elems)++;
            }
        }
    }

    // Close the file
    fclose(file);

    return 0; // Return 0 to indicate success

}

int read_mesh(const char* filename, const int* nb_nodes,const int* nb_elems, int* node_id, int* elem_id, int* je, double* coord){


    // internal variables definition
    FILE *file;                          // file handler
    char line[MAX_LINE_LENGTH];          // file record (line based)
    int global_node_id;                  // global node id at a given record
    double coordX, coordY;               // coordinates X and Y
    int global_elem_id;                  // global element id at a given record
    int node1, node2, node3;             // Tri3 element incidence

    // Open the file in read mode
    file = fopen(filename, "r");
    if (file == NULL) {
        printf("Failed to open the file.\n");
        return 1; // Return an error code indicating failure
    }


    int iNode = 0;
    int iElem = 0;
    while (fgets(line, sizeof(line), file) != NULL) {
        int value;
        if (sscanf(line, "%d", &value) == 1){
            if (value == 1) {
                if (sscanf(line, "%*d %d %*d %lf %lf", &global_node_id, &coordX, &coordY) == 3) {
                    node_id[iNode]   = global_node_id;
                    coord[2*iNode]   = coordX;
                    coord[2*iNode+1] = coordY;
                    iNode++;
                }
            } else if (value == 2){
                if (sscanf(line, "%*d %d %*d %*d %d %d %d", &global_elem_id, &node1, &node2, &node3) == 4) {
                   elem_id[iElem]  = global_elem_id;
                   je[3*iElem]     = node1;
                   je[3*iElem + 1] = node2;
                   je[3*iElem + 2] = node3;
                   iElem++; 
                }
            }
        }
    }

    // Close the file
    fclose(file);

    return 0; // Return 0 to indicate success

}