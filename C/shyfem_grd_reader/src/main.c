#include <stdio.h>
#include <string.h>
#include "grd_reader.h"

int main(int argc, char *argv[]) {


    // variables definition
    int global_node_id;
    int global_elem_id;
    double coordX, coordY;
    int node1, node2, node3;
    int i, j;
    
    if (argc != 2) {
        printf("Usage: mesh_reader.bin mesh.grd\n");
        return 1;
    }
    
    FILE *file;
    char line[MAX_LINE_LENGTH];
    int count = 0;
    int nb_nodes = 0;
    int nb_elems = 0;

    // Open the file in read mode
    file = fopen(argv[1], "r");
    if (file == NULL) {
        printf("Failed to open the file.\n");
        return 1;
    }

    // Read each line and check if it starts with "1" or "2"
    while (fgets(line, sizeof(line), file) != NULL) {
        // Use sscanf to extract the first field as an integer
        int value;
        if (sscanf(line, "%d", &value) == 1) {
        
            if (value == 1) {
                //count++;
                nb_nodes++;
            } else if (value == 2) {
                nb_elems++;
            }
        }
    }

    
    printf("Number of nodes : %d\n", nb_nodes);
    printf("Number of elems : %d\n", nb_elems);

    int node_id[nb_nodes];
    double node_coord[2*nb_nodes];
    int elem_id[nb_elems];
    int je[3*nb_elems];

    rewind(file);
    int iNode = 0;
    int iElem = 0;
    while (fgets(line, sizeof(line), file) != NULL) {
        int value;
        if (sscanf(line, "%d", &value) == 1){
            if (value == 1) {
                if (sscanf(line, "%*d %d %*d %lf %lf", &global_node_id, &coordX, &coordY) == 3) {
                    node_id[iNode] = global_node_id;
                    node_coord[2*iNode]   = coordX;
                    node_coord[2*iNode+1] = coordY;
                    iNode++;
                }
            } else if (value == 2){
                if (sscanf(line, "%*d %d %*d %*d %d %d %d", &global_elem_id, &node1, &node2, &node3) == 4) {
                   elem_id[iElem] = global_elem_id;
                   je[3*iElem]     = node1;
                   je[3*iElem + 1] = node2;
                   je[3*iElem + 2] = node3;
                   iElem++; 
                }
            }
        }
    }    



    // Close the file
    fclose(file);


    int ierr = get_dims(argv[1], &nb_nodes, &nb_elems);

    printf("Number of nodes : %d\n", nb_nodes);
    printf("Number of elems : %d\n", nb_elems);

    
    // clean memory
    for (i = 0; i < nb_nodes; i++) {
        node_id[i] = 0;
        node_coord[2*i]   = 0.0;
        node_coord[2*i+1] = 0.0;
    }
    for (i = 0; i < nb_elems; i++) {
        je[3*i]     = 0;
        je[3*i + 1] = 0;
        je[3*i + 2] = 0;
    }

    ierr = read_mesh(argv[1],&nb_nodes, &nb_elems, node_id, elem_id, je, node_coord);

    //
    printf("node_id: ");
    for (i = 0; i < nb_nodes; i++) {
        printf("%d ", node_id[i]);
    }
    printf("\n");

    printf("je: \n");
    for (i = 0; i < nb_elems; i++) {
        for (j = 0; j < 3; j++) {
            printf("%8d ", je[3*i+j]);
        }
        printf("\n");
    }
    printf("\n");


    return 0;
}

