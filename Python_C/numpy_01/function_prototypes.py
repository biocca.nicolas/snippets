import numpy as np
import ctypes

mylib = ctypes.CDLL('lib/mylib.so')


# function prototypes
mylib.square_elements.argtypes = [ctypes.POINTER(ctypes.c_double),  # pass by reference
                                  ctypes.c_size_t,                  # pass by value
                                  ctypes.c_size_t]                  # pass by value

mylib.square_elements.restypes = None                               # void function


def square_elements(arr):
    if len(arr.shape) != 2:
        raise ValueError("Input array must be 2D")

    # Get dimensions of the input array
    rows, cols = arr.shape

    # Convert the NumPy array to a C array
    #c_arr = (ctypes.c_double * (rows * cols))(*arr.flatten())
    c_arr = np.ctypeslib.as_ctypes(arr.flatten())

    # Call the C function (void function)
    mylib.square_elements(c_arr, ctypes.c_size_t(rows), ctypes.c_size_t(cols))

    # Convert the result back to a NumPy array (flatten)
    result_arr = np.ctypeslib.as_array(c_arr)

    return result_arr.reshape(arr.shape)

