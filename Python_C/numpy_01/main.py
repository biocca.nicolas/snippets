import sys
import numpy as np
from function_prototypes import square_elements 

def main():

  # Create a sample 2D NumPy array
  arr = np.array([[1.0, 2.0, 3.0], [4.0, 5.0, 6.0]])
  print(f'input numpy array:\n{arr}')
  
  # Call the square_elements function
  result = square_elements(arr)
  print(f'output numpy array:\n{result}')


if __name__ == '__main__':
    main()
