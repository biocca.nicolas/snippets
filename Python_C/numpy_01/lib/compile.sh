#!/bin/bash

rm mylib.so

gcc -Wall -fPIC -shared -o mylib.so square_elements.c

#Check if the compilation was successful
if [ $? -eq 0 ]; then
    echo "Compilation successful."
else
    echo "Compilation failed."
fi
