//
// Compile this function and wrap into a shared library by
//
// gcc -Wall -fPIC -shared -o mylibrary.so square_elements.c
//

#include <stddef.h>

void square_elements(double* input_array, size_t rows, size_t cols) {
  for (size_t i = 0; i < rows; i++) {
    for (size_t j = 0; j < cols; j++) {
        double value = input_array[i * cols + j];
        input_array[i * cols + j] = value * value; // Square the element in-place
    }
  }
}

