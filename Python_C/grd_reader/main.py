import os, sys
import ctypes
import numpy as np

# Load the shared library
grdreader = ctypes.CDLL('lib/mylib.so')

# Function prototypes
grdreader.get_dims.argtypes = [ctypes.c_char_p, ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_int)]
grdreader.read_mesh.argtypes = [ctypes.c_char_p, ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_double)]

# Define the argument and return types
grdreader.get_dims.restype = ctypes.c_int
grdreader.read_mesh.restype = ctypes.c_int

# Example usage
filename = '/home/nico/Documents/CMCC/grd_reader_c/test.grd'
if not os.path.isfile(filename):
    sys.exit(f'File {filename}\nDoes not exist')

nb_nodes = ctypes.c_int()
nb_elems = ctypes.c_int()

# Call the get_dims function
result = grdreader.get_dims(filename.encode(), ctypes.byref(nb_nodes), ctypes.byref(nb_elems))
if result == 0:
    print("Number of nodes:", nb_nodes.value)
    print("Number of elements:", nb_elems.value)
else:
    print("Failed to get dimensions.")


# Example usage of read_mesh function
node_id = (ctypes.c_int * nb_nodes.value)()
elem_id = (ctypes.c_int * nb_elems.value)()
je = (ctypes.c_int * (3 * nb_elems.value))()
coord = (ctypes.c_double * (2 * nb_nodes.value))()

result = grdreader.read_mesh(filename.encode(), ctypes.byref(nb_nodes), ctypes.byref(nb_elems), node_id, elem_id, je, coord)
if result == 0:
    print("Mesh read successfully.")
else:
    print("Failed to read mesh.")

# Define NumPy arrays from the variables
node_id_array = np.array(node_id)
elem_id_array = np.array(elem_id)
je_array = np.array(je)
coord_array = np.array(coord)



