
#define MAX_LINE_LENGTH 256
#define NDIM 2

int get_dims(const char* filename, int* nb_nodes, int* nb_elems);
int read_mesh(const char* filename, const int* nb_nodes, const int* nb_elems, int* node_id, int* elem_id, int* je, double* coord);

