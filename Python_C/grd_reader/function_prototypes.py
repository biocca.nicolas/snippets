import sys
import ctypes
import numpy as np

# Load the shared library
grdreader = ctypes.CDLL('lib/mylib.so')

# get_dim
grdreader.get_dims.argtypes = [ctypes.c_char_p,                     # filename
                               ctypes.POINTER(ctypes.c_int),        # nb_nodes
                               ctypes.POINTER(ctypes.c_int)]        # nb_elems

grdreader.get_dims.restype = ctypes.c_int                           # ierr

# read_mesh
grdreader.read_mesh.argtypes = [ctypes.c_char_p,                    # filename
                                ctypes.POINTER(ctypes.c_int),       # nb_nodes
                                ctypes.POINTER(ctypes.c_int),       # nb_elems
                                ctypes.POINTER(ctypes.c_int),       # node_id
                                ctypes.POINTER(ctypes.c_int),       # elem_id
                                ctypes.POINTER(ctypes.c_int),       # je
                                ctypes.POINTER(ctypes.c_double)]    # coord

grdreader.read_mesh.restype = ctypes.c_int                          # ierrs


def get_dims(filename:str):
    '''
        Wrapper get_dims C function defined in mylib.so
    '''
    nb_nodes = ctypes.c_int()
    nb_elems = ctypes.c_int()

    # Call the get_dims function
    ierr = grdreader.get_dims(filename.encode(), ctypes.byref(nb_nodes), ctypes.byref(nb_elems))

    if (ierr != 0):
        sys.exit('error: get_dims function failed')
    
    return nb_nodes.value, nb_elems.value

def read_mesh(filename:str):
    '''
        Wrapper read_mesh C function defined in mylib.so. It automatically
        triggers get_dims to get nb_nodes and nb_elems
    '''

    nb_nodes, nb_elems = get_dims(filename)

    
    nb_nodes = ctypes.c_int(nb_nodes)
    nb_elems = ctypes.c_int(nb_elems)

    # allocate memory
    node_id = (ctypes.c_int * nb_nodes.value)()
    elem_id = (ctypes.c_int * nb_elems.value)()
    je      = (ctypes.c_int * (3 * nb_elems.value))()
    coord   = (ctypes.c_double * (2 * nb_nodes.value))()

    ierr = grdreader.read_mesh(filename.encode(), 
                               ctypes.byref(nb_nodes),
                               ctypes.byref(nb_elems),
                               node_id,
                               elem_id,
                               je,
                               coord)
    
    if (ierr != 0):
        sys.exit('error: read_mesh function failed')
    
    return nb_nodes.value, nb_elems.value, np.array(node_id), np.array(elem_id), np.array(je).reshape((-1,nb_elems.value)), np.array(coord).reshape((-1,nb_nodes.value))

