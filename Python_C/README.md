# Python/C Interoperability

This collection of snippets demonstrates the seemless integration of Python and C, showcasing how to leverage the power of both languages in a single project.

## Structure

- **numpy_01**: computes element-wise squares for 2-dimensional numpy array in a C library. 
  - [Link to numpy_01](numpy_01/)
 
- **grd_reader**: read grd shyfem mesh using by means of C library.  
  - [Link to grd_reader](grd_reader/)
