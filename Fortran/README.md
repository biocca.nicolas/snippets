# Fortran

Welcome to the Fortran snippets directory! This directory contains a collection of Fortran code snippets, each organized in its own subdirectory. These snippets cover various Fortran programming concepts and can be helpful for learning, reference, and reusability as well. In addition, some interoperability tricks are written, in particular those regarding Python/C. 


## Structure

- **bas_reader**: Read a binary bas file from SHYFEM mesh. This is not really just a snippet, it contains a class definition for the reading of shyfem meshes defined in bas format. It also include an interface to Python. 
  - [Link to bas_reader](bas_reader/)
