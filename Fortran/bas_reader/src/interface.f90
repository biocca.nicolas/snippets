!
! C like interface for interoperability with C/C++/Python
!

subroutine instanciate(filename) BIND(c,name='instanciate')
    USE mod_bas
    USE ISO_C_BINDING
    IMPLICIT NONE
    CHARACTER(C_CHAR), DIMENSION(*), INTENT(IN) :: filename
    CHARACTER(LEN=256) :: f_string
    INTEGER(4) :: i
    INTEGER(4) :: str_len

    ! Determine the length of the message by finding the null terminator
    str_len = 0
    DO WHILE (filename(str_len + 1) /= c_null_char)
       str_len = str_len + 1
    ENDDO

    DO i=1, str_len
        f_string(i:i) = filename(i) 
    ENDDO
    f_string(str_len+1:) = ' '
    
    CALL mesh2%read_mesh(f_string)
end subroutine 

SUBROUTINE get_ftype(ftype) BIND(C,name='get_ftype')
    USE mod_bas
    USE ISO_C_BINDING
    IMPLICIT NONE
    INTEGER(C_INT) :: ftype
    ftype = mesh2%get_ftype()
END SUBROUTINE 

SUBROUTINE get_nvers(nvers) BIND(C,name='get_nvers')
    USE mod_bas
    USE ISO_C_BINDING
    IMPLICIT NONE
    INTEGER(C_INT) :: nvers
    nvers = mesh2%get_nvers()
END SUBROUTINE 

SUBROUTINE get_nkn(nkn) BIND(C,name='get_nkn')
    USE mod_bas
    USE ISO_C_BINDING
    IMPLICIT NONE
    INTEGER(C_INT) :: nkn
    nkn = mesh2%get_nkn()
END SUBROUTINE 

SUBROUTINE get_nel(nel) BIND(C,name='get_nel')
    USE mod_bas
    USE ISO_C_BINDING
    IMPLICIT NONE
    INTEGER(C_INT) :: nel
    nel = mesh2%get_nel()
END SUBROUTINE 

SUBROUTINE get_ngr(ngr) BIND(C,name='get_ngr')
    USE mod_bas
    USE ISO_C_BINDING
    IMPLICIT NONE
    INTEGER(C_INT) :: ngr
    ngr = mesh2%get_ngr()
END SUBROUTINE 

SUBROUTINE get_mbw(mbw) BIND(C,name='get_mbw')
    USE mod_bas
    USE ISO_C_BINDING
    IMPLICIT NONE
    INTEGER(C_INT) :: mbw
    mbw = mesh2%get_mbw()
END SUBROUTINE 

SUBROUTINE get_dcorbas(dcorbas) BIND(C,name='get_dcorbas')
    USE mod_bas
    USE ISO_C_BINDING
    IMPLICIT NONE
    REAL(FP) :: dcorbas
    dcorbas = mesh2%get_dcorbas()
END SUBROUTINE 

SUBROUTINE get_dirnbas(dirnbas) BIND(C,name='get_dirnbas')
    USE mod_bas
    USE ISO_C_BINDING
    IMPLICIT NONE
    REAL(FP) :: dirnbas
    dirnbas = mesh2%get_dirnbas()
END SUBROUTINE 

SUBROUTINE get_descrr(descrr) BIND(C,name='get_descrr')
    USE mod_bas
    USE ISO_C_BINDING
    IMPLICIT NONE
    CHARACTER(C_CHAR), DIMENSION(*) :: descrr 

    CHARACTER(LEN=80) :: f_string
    INTEGER(4) :: i

    f_string = mesh2%get_descrr()

    DO i=1, LEN(f_string)-1
        descrr(i) = f_string(i:i)
    ENDDO
    descrr(LEN(f_string)) = C_NULL_CHAR
END SUBROUTINE 

SUBROUTINE get_nen3v(nen3v) BIND(C,name='get_nen3v')
    USE mod_bas
    USE ISO_C_BINDING
    IMPLICIT NONE
    INTEGER(C_INT), DIMENSION(*) :: nen3v
    INTEGER(4), ALLOCATABLE :: p_int(:,:)
    INTEGER(4) :: n_size
    INTEGER(4) :: i, j, k

    n_size = mesh2%get_nel()
    ALLOCATE(p_int(3,n_size))
    p_int = mesh2%get_nen3v()
    DO i=1, n_size
        DO j=1, 3
            k = (i-1)*3 + j
            nen3v(k) =  p_int(j,i)
        ENDDO
    ENDDO
    DEALLOCATE(p_int)
END SUBROUTINE 

SUBROUTINE get_ipv(ipv) BIND(C,name='get_ipv')
    USE mod_bas
    USE ISO_C_BINDING
    IMPLICIT NONE
    INTEGER(C_INT), DIMENSION(*) :: ipv
    INTEGER(4), ALLOCATABLE :: p_int(:)
    INTEGER(4) :: n_size
    INTEGER(4) :: i

    n_size = mesh2%get_nkn()
    ALLOCATE(p_int(n_size))
    p_int = mesh2%get_ipv()
    DO i=1, n_size
        ipv(i) =  p_int(i)
    ENDDO
    DEALLOCATE(p_int)
END SUBROUTINE 

SUBROUTINE get_ipev(ipev) BIND(C,name='get_ipev')
    USE mod_bas
    USE ISO_C_BINDING
    IMPLICIT NONE
    INTEGER(C_INT), DIMENSION(*) :: ipev
    INTEGER(4), ALLOCATABLE :: p_int(:)
    INTEGER(4) :: n_size
    INTEGER(4) :: i

    n_size = mesh2%get_nel()
    ALLOCATE(p_int(n_size))
    p_int = mesh2%get_ipev()
    DO i=1, n_size
        ipev(i) =  p_int(i)
    ENDDO
    DEALLOCATE(p_int)
END SUBROUTINE 

SUBROUTINE get_xgv(xgv) BIND(C,name='get_xgv')
    USE mod_bas
    USE ISO_C_BINDING
    IMPLICIT NONE
    REAL(C_DOUBLE), DIMENSION(*) :: xgv
    REAL(FP), ALLOCATABLE :: p_float(:)
    INTEGER(4) :: n_size
    INTEGER(4) :: i

    n_size = mesh2%get_nkn()
    ALLOCATE(p_float(n_size))
    p_float = mesh2%get_xgv()
    DO i=1, n_size
        xgv(i) = DBLE(p_float(i))
    ENDDO
    DEALLOCATE(p_float)
END SUBROUTINE 

SUBROUTINE get_ygv(ygv) BIND(C,name='get_ygv')
    USE mod_bas
    USE ISO_C_BINDING
    IMPLICIT NONE
    REAL(C_DOUBLE), DIMENSION(*) :: ygv
    REAL(FP), ALLOCATABLE :: p_float(:)
    INTEGER(4) :: n_size
    INTEGER(4) :: i

    n_size = mesh2%get_nkn()
    ALLOCATE(p_float(n_size))
    p_float = mesh2%get_ygv()
    DO i=1, n_size
        ygv(i) = DBLE(p_float(i))
    ENDDO
    DEALLOCATE(p_float)
END SUBROUTINE 

SUBROUTINE get_hm3v(hm3v) BIND(C,name='get_hm3v')
    USE mod_bas
    USE ISO_C_BINDING
    IMPLICIT NONE
    REAL(C_DOUBLE), DIMENSION(*) :: hm3v
    REAL(FP), ALLOCATABLE :: p_float(:,:)
    INTEGER(4) :: n_size
    INTEGER(4) :: i, j, k

    n_size = mesh2%get_nel()
    ALLOCATE(p_float(3,n_size))
    p_float = mesh2%get_hm3v()
    DO i=1, n_size
        DO j=1, 3
            k = (i-1)*3 + j
            hm3v(k) =  DBLE(p_float(j,i))
        ENDDO
    ENDDO
    DEALLOCATE(p_float)
END SUBROUTINE 

