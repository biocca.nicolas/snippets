program bas_reader

    use mod_bas
    implicit none

    type(mesh)  :: mesh1
    integer(4)  :: ierr
    
    integer(4) :: intVar
    integer(4), POINTER :: pIntVar(:)

    integer(4) :: i

    character(LEN=256) :: filename
    filename = 'test/mesh.bas'

    CALL test01
    STOP

    CALL GET_COMMAND_ARGUMENT(1,filename)
    WRITE(*,*) filename
    CALL mesh1%read_mesh(filename)
    
    pIntVar => mesh1%get_ipv()


    write(*,*) 'info: writing ipv'
    do i=1, 5
        write(*,*) pIntVar(i)
    enddo

    CONTAINS

    SUBROUTINE test01
        IMPLICIT NONE
        TYPE(mesh) :: mesh1
        CHARACTER(len=256) :: filename

        WRITE(*,*) 'info: unit test 01'

        filename = '../test/mesh.bas'

        CALL mesh1%read_mesh(filename)

        write(*,*) 'get_ftype   = ', mesh1%get_ftype()
        write(*,*) 'get_nvers   = ', mesh1%get_nvers()
        write(*,*) 'get_nkn     = ', mesh1%get_nkn()
        write(*,*) 'get_nel     = ', mesh1%get_nel()
        write(*,*) 'get_ngr     = ', mesh1%get_ngr()
        write(*,*) 'get_mbw     = ', mesh1%get_mbw()
        write(*,*) 'get_dcorbas = ', mesh1%get_dcorbas()
        write(*,*) 'get_dirnbas = ', mesh1%get_dirnbas()
        write(*,*) 'get_descrr  = ', mesh1%get_descrr()
        write(*,*) 'get_nen3v   = ', mesh1%get_nen3v()
        write(*,*) 'get_ipv     = ', mesh1%get_ipv()
        write(*,*) 'get_ipev    = ', mesh1%get_ipev()
        WRITE(*,*) 'get_iarv    = ', mesh1%get_iarv()
        WRITE(*,*) 'get_xgv     = ', mesh1%get_xgv()
        WRITE(*,*) 'get_ygv     = ', mesh1%get_ygv()
        WRITE(*,*) 'get_hm3v    = ', mesh1%get_hm3v()


    END SUBROUTINE

end program


