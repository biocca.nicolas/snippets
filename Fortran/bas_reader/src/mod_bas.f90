MODULE mod_bas
    IMPLICIT NONE
    PRIVATE

    INTEGER(4),         PARAMETER :: fsp = 4
    INTEGER(4),         PARAMETER :: fdp = 8
    INTEGER(4), PUBLIC, PARAMETER :: fp = fsp   ! float precision 

    TYPE, PUBLIC :: mesh

        PRIVATE 
        INTEGER(4) :: ftype
        INTEGER(4) :: nvers
        
        INTEGER(4) :: nkn
        INTEGER(4) :: nel
        INTEGER(4) :: ngr
        INTEGER(4) :: mbw
        
        REAL(fp) :: dcorbas
        REAL(fp) :: dirnbas

        CHARACTER(80) :: descrr

        INTEGER(4), ALLOCATABLE  :: nen3v(:,:)
        INTEGER(4), ALLOCATABLE  :: ipv(:)
        INTEGER(4), ALLOCATABLE  :: ipev(:)
        INTEGER(4), ALLOCATABLE  :: iarv(:)
        

        REAL(fp), allocatable :: xgv(:)
        REAL(fp), allocatable :: ygv(:)
        REAL(fp), allocatable :: hm3v(:,:)


    CONTAINS

        PROCEDURE :: read_mesh => read_bas_file

        PROCEDURE :: get_ftype => get_ftype
        PROCEDURE :: get_nvers => get_nvers
        PROCEDURE :: get_nkn => get_nkn
        PROCEDURE :: get_nel => get_nel
        PROCEDURE :: get_ngr => get_ngr
        PROCEDURE :: get_mbw => get_mbw
        PROCEDURE :: get_dcorbas => get_dcorbas
        PROCEDURE :: get_dirnbas => get_dirnbas
        PROCEDURE :: get_descrr => get_descrr
        PROCEDURE :: get_nen3v => get_nen3v
        PROCEDURE :: get_ipv => get_ipv
        PROCEDURE :: get_ipev => get_ipev
        PROCEDURE :: get_iarv => get_iarv
        PROCEDURE :: get_xgv => get_xgv
        PROCEDURE :: get_ygv => get_ygv
        PROCEDURE :: get_hm3v => get_hm3v

    ENDTYPE

    INTEGER(4),     PUBLIC :: iounit = 15


    TYPE(mesh), PUBLIC, TARGET :: mesh2 

    CONTAINS

    ! getters
    FUNCTION get_ftype(this) result(ftype)
        CLASS(mesh), INTENT(IN) :: this
        INTEGER(4) :: ftype
        ftype = this%ftype
    END FUNCTION

    FUNCTION get_nvers(this) result(nvers)
        CLASS(mesh), INTENT(IN) :: this
        INTEGER(4) :: nvers
        nvers = this%nvers
    END FUNCTION

    FUNCTION get_nkn(this) result(nkn)
        CLASS(mesh), INTENT(IN) :: this
        INTEGER(4) :: nkn
        nkn = this%nkn
    END FUNCTION

    FUNCTION get_nel(this) result(nel)
        CLASS(mesh), INTENT(IN) :: this
        INTEGER(4) :: nel
        nel = this%nel
    END FUNCTION

    FUNCTION get_ngr(this) result(ngr)
        CLASS(mesh), INTENT(IN) :: this
        INTEGER(4) :: ngr
        ngr = this%ngr
    END FUNCTION

    FUNCTION get_mbw(this) result(mbw)
        CLASS(mesh), INTENT(IN) :: this
        INTEGER(4) :: mbw
        mbw = this%mbw
    END FUNCTION

    FUNCTION get_dcorbas(this) result(dcorbas)
        CLASS(mesh), INTENT(IN) :: this
        REAL(fp) :: dcorbas
        dcorbas = this%dcorbas
    END FUNCTION

    FUNCTION get_dirnbas(this) result(dirnbas)
        CLASS(mesh), INTENT(IN) :: this
        REAL(fp) :: dirnbas
        dirnbas = this%dirnbas
    END FUNCTION

    FUNCTION get_descrr(this) result(descrr)
        CLASS(mesh), INTENT(IN) :: this
        CHARACTER(80) :: descrr
        descrr = this%descrr
    END FUNCTION

    FUNCTION get_nen3v(this) result(nen3v)
        CLASS(mesh), INTENT(IN), TARGET :: this
        INTEGER(4), POINTER :: nen3v(:,:)
        nen3v => this%nen3v
    END FUNCTION

    FUNCTION get_ipv(this) result(ipv)
        CLASS(mesh), INTENT(IN), TARGET :: this
        INTEGER(4), POINTER :: ipv(:)
        ipv => this%ipv
    END FUNCTION

    FUNCTION get_ipev(this) result(ipev)
        CLASS(mesh), INTENT(IN), TARGET :: this
        INTEGER(4), POINTER :: ipev(:)
        ipev => this%ipev
    END FUNCTION

    FUNCTION get_iarv(this) result(iarv)
        CLASS(mesh), INTENT(IN), TARGET :: this
        INTEGER(4), POINTER :: iarv(:)
        iarv => this%iarv
    END FUNCTION

    FUNCTION get_xgv(this) result(xgv)
        CLASS(mesh), INTENT(IN), TARGET :: this
        REAL(fp), POINTER :: xgv(:)
        xgv => this%xgv
    END FUNCTION

    FUNCTION get_ygv(this) result(ygv)
        CLASS(mesh), INTENT(IN), TARGET :: this
        REAL(fp), POINTER :: ygv(:)
        ygv => this%ygv
    END FUNCTION

    FUNCTION get_hm3v(this) result(hm3v)
        CLASS(mesh), INTENT(IN), TARGET :: this
        REAL(fp), POINTER :: hm3v(:,:)
        hm3v => this%hm3v
    END FUNCTION

    FUNCTION allocate_arrays(this) result(ierr)

        IMPLICIT NONE
        CLASS(mesh), INTENT(INOUT) :: this
        INTEGER(4) :: ierr

        ASSOCIATE(nel => this%nel, nkn => this%nkn)
            ALLOCATE(this%nen3v(3,nel))
            ALLOCATE(this%ipv(nkn))
            ALLOCATE(this%ipev(nel))
            ALLOCATE(this%iarv(nel))
            ALLOCATE(this%xgv(nkn))
            ALLOCATE(this%ygv(nkn))
            ALLOCATE(this%hm3v(3,nel))
        END ASSOCIATE

        ierr = 0
    END FUNCTION

    SUBROUTINE read_bas_file(this,meshfile) 

        IMPLICIT NONE
        CLASS(mesh), INTENT(INOUT) :: this
        CHARACTER(LEN=256), INTENT(IN) :: meshfile

        INTEGER(4) :: i, ii
        INTEGER(4) :: ierr
       
        write(*,*) 'Opening file:', TRIM(meshfile)
        OPEN(UNIT=iounit,         &
             FILE=TRIM(meshfile), &
             ACCESS='sequential', &
             FORM='unformatted') 

        REWIND(iounit)

        READ(iounit) this%ftype, this%nvers
        READ(iounit) this%nkn, this%nel, this%ngr, this%mbw
        READ(iounit) this%dcorbas, this%dirnbas
        READ(iounit) this%descrr

        ierr = allocate_arrays(this)

        READ(iounit)((this%nen3v(ii,i),ii=1,3),i=1,this%nel)
        READ(iounit)(this%ipv(i),i=1,this%nkn)
        READ(iounit)(this%ipev(i),i=1,this%nel)
        READ(iounit)(this%iarv(i),i=1,this%nel)
        
        READ(iounit)(this%xgv(i),i=1,this%nkn)
        READ(iounit)(this%ygv(i),i=1,this%nkn)
        READ(iounit)((this%hm3v(ii,i),ii=1,3),i=1,this%nel)

        CLOSE(iounit)

    END SUBROUTINE 

END MODULE
