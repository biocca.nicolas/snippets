#
# Function prototypes
#

import ctypes
import numpy as np

libbasmesh = ctypes.CDLL('lib/libbasmesh.so')
    
#instanciate
def instanciate(filename_bas:str) -> None:
    libbasmesh.instanciate.argtypes = [ctypes.c_char_p]
    libbasmesh.instanciate.restype = ctypes.c_void_p
    libbasmesh.instanciate(filename_bas.encode())

# get_ftype
def get_ftype() -> int:
    libbasmesh.get_ftype.argtypes = [ctypes.POINTER(ctypes.c_int)]
    libbasmesh.get_ftype.restype = ctypes.c_void_p
    ftype = ctypes.c_int()
    libbasmesh.get_ftype(ctypes.byref(ftype))
    return ftype.value

# get_nvers
def get_nvers() -> int:
    libbasmesh.get_nvers.argtypes = [ctypes.POINTER(ctypes.c_int)]
    libbasmesh.get_nvers.restype = ctypes.c_void_p
    nvers = ctypes.c_int()
    libbasmesh.get_nvers(ctypes.byref(nvers))
    return nvers.value

# get_nkn
def get_nkn() -> int:
    libbasmesh.get_nkn.argtypes = [ctypes.POINTER(ctypes.c_int)]
    libbasmesh.get_nkn.restype = ctypes.c_void_p
    nkn = ctypes.c_int()
    libbasmesh.get_nkn(ctypes.byref(nkn))
    return nkn.value

# get_nel
def get_nel() -> int:
    libbasmesh.get_nel.argtypes = [ctypes.POINTER(ctypes.c_int)]
    libbasmesh.get_nel.restype = ctypes.c_void_p
    nel = ctypes.c_int()
    libbasmesh.get_nel(ctypes.byref(nel))
    return nel.value

# get_ngr
def get_ngr() -> int:
    libbasmesh.get_ngr.argtypes = [ctypes.POINTER(ctypes.c_int)]
    libbasmesh.get_ngr.restype = ctypes.c_void_p
    ngr = ctypes.c_int()
    libbasmesh.get_ngr(ctypes.byref(ngr))
    return ngr.value

# get_mbw
def get_mbw() -> int:
    libbasmesh.get_mbw.argtypes = [ctypes.POINTER(ctypes.c_int)]
    libbasmesh.get_mbw.restype = ctypes.c_void_p
    mbw = ctypes.c_int()
    libbasmesh.get_mbw(ctypes.byref(mbw))
    return mbw.value

# get_dcorbas
def get_dcorbas() -> float:
    libbasmesh.get_dcorbas.argtypes = [ctypes.POINTER(ctypes.c_float)]
    libbasmesh.get_dcorbas.restype = ctypes.c_void_p
    dcorbas = ctypes.c_float()
    libbasmesh.get_dcorbas(ctypes.byref(dcorbas))
    return dcorbas.value

# get_dirnbas
def get_dirnbas() -> float:
    libbasmesh.get_dirnbas.argtypes = [ctypes.POINTER(ctypes.c_float)]
    libbasmesh.get_dirnbas.restype = ctypes.c_void_p
    dirnbas = ctypes.c_float()
    libbasmesh.get_dirnbas(ctypes.byref(dirnbas))
    return dirnbas.value

# get_descrr
def get_descrr() -> str:
    libbasmesh.get_descrr.argtypes = [ctypes.c_char_p]
    libbasmesh.get_descrr.restype = ctypes.c_void_p
    BUFFER_SIZE = 80
    descrr = ctypes.create_string_buffer(BUFFER_SIZE)
    libbasmesh.get_descrr(descrr)
    return descrr.value.decode()

# get_nen3v
def get_nen3v() -> np.ndarray:
    libbasmesh.get_nen3v.argtypes = [ctypes.POINTER(ctypes.c_int)]
    libbasmesh.get_nen3v.restype = ctypes.c_void_p
    nb_elem = get_nel()
    nen3v = (ctypes.c_int * (3*nb_elem))()
    libbasmesh.get_nen3v(nen3v)
    return np.array(nen3v).reshape((-1,3))

# get_ipv
def get_ipv() -> np.ndarray:
    libbasmesh.get_ipv.argtypes = [ctypes.POINTER(ctypes.c_int)]
    libbasmesh.get_ipv.restype = ctypes.c_void_p
    nb_node = get_nkn()
    ipv = (ctypes.c_int * nb_node)()
    libbasmesh.get_ipv(ipv)
    return np.array(ipv)

# get_ipev
def get_ipev() -> np.ndarray:
    libbasmesh.get_ipev.argtypes = [ctypes.POINTER(ctypes.c_int)]
    libbasmesh.get_ipev.restype = ctypes.c_void_p
    nb_elem = get_nel()
    ipev = (ctypes.c_int * nb_elem)()
    libbasmesh.get_ipev(ipev)
    return np.array(ipev)

# get_xgv
def get_xgv() -> np.ndarray:
    libbasmesh.get_xgv.argtypes = [ctypes.POINTER(ctypes.c_double)]
    libbasmesh.get_xgv.restype = ctypes.c_void_p
    nb_node = get_nkn()
    xgv = (ctypes.c_double * nb_node)()
    libbasmesh.get_xgv(xgv)
    return np.array(xgv)

# get_ygv
def get_ygv() -> np.ndarray:
    libbasmesh.get_ygv.argtypes = [ctypes.POINTER(ctypes.c_double)]
    libbasmesh.get_ygv.restype = ctypes.c_void_p
    nb_node = get_nkn()
    ygv = (ctypes.c_double * nb_node)()
    libbasmesh.get_ygv(ygv)
    return np.array(ygv)

# get_hm3v
def get_hm3v() -> np.ndarray:
    libbasmesh.get_hm3v.argtypes = [ctypes.POINTER(ctypes.c_double)]
    libbasmesh.get_hm3v.restype = ctypes.c_void_p
    nb_elem = get_nel()
    hm3v = (ctypes.c_double * (3*nb_elem))()
    libbasmesh.get_hm3v(hm3v)
    return np.array(hm3v).reshape((-1,3))
