

def test_01():
    import function_prototypes as basmesh
    
    basmesh.instanciate('test/mesh.bas')

    print(f'{basmesh.get_ftype()=}')
    print(f'{basmesh.get_nvers()=}')
    print(f'{basmesh.get_nkn()=}')
    print(f'{basmesh.get_nel()=}')
    print(f'{basmesh.get_ngr()=}')
    print(f'{basmesh.get_mbw()=}')
    print(f'{basmesh.get_dcorbas()=}')
    print(f'{basmesh.get_dirnbas()=}')
    print(f'{basmesh.get_descrr()=}')
    print(f'{basmesh.get_nen3v()=}')
    print(f'{basmesh.get_ipv()=}')
    print(f'{basmesh.get_ipev()=}')
    print(f'{basmesh.get_xgv()=}')
    print(f'{basmesh.get_ygv()=}')
    print(f'{basmesh.get_hm3v()=}')

if __name__ == '__main__':

    print(f'Testing lib interface')
    test_01()
