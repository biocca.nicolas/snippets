#!/bin/bash

#
# Translate grd (shyfem) mesh to vtk (Legacy ASCII v 3.0) mesh
#
# Author: Nicolás Biocca


GRD_FILE=$1
VTK_FILE=$(basename -s .grd $GRD_FILE).vtk

echo "info: starting grd2msh script"
echo "input file  = $GRD_FILE"
echo "output file = $VTK_FILE"

NB_NODES=$(awk '{if ($1 == 1) i++;} END {print i}' $GRD_FILE)
NB_ELEMS=$(awk '{if ($1 == 2) i++;} END {print i}' $GRD_FILE)
NB_LINES=$(awk '{if ($1 == 3) i++;} END {print i}' $GRD_FILE)

echo "# vtk DataFile Version 3.0" > $VTK_FILE
echo "This file was written by grd2vtk script" >> $VTK_FILE
echo "ASCII" >> $VTK_FILE
echo "DATASET UNSTRUCTURED_GRID" >> $VTK_FILE
echo "POINTS $NB_NODES double" >> $VTK_FILE 

awk '{if ($1 == 1) print $4, $5, 0}' $GRD_FILE >> $VTK_FILE

echo "CELLS $NB_ELEMS $(($NB_ELEMS*(3+1)))" >> $VTK_FILE
awk '{if ($1 == 2) print $4, $5-1, $6-1, $7-1}' $GRD_FILE >> $VTK_FILE   # vtk uses zero-based numbering

echo "CELL_TYPES $NB_ELEMS" >> $VTK_FILE
awk '{if ($1 == 2) print 5}' $GRD_FILE >> $VTK_FILE     # VKT_ID = 5 correspond to Tri3 element

# Comment is no bathymetry is given at cells
NB_ELEMS_BATHY=$(awk 'BEGIN {i = 0} {if ($1 == 2 && NF == 8) i++;} END {print i}' $GRD_FILE)   
if [ $NB_ELEMS_BATHY -eq $NB_ELEMS ]; then
    echo "CELL_DATA $NB_ELEMS" >> $VTK_FILE
    echo "SCALARS depth double" >> $VTK_FILE 
    echo "LOOKUP_TABLE default" >> $VTK_FILE 
    awk '{if ($1 == 2 && NF == 8) print $8}' $GRD_FILE >> $VTK_FILE   
fi

echo "info: done"
