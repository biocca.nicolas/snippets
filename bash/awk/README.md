# AWK 

AWK is a versatile text-processing tool that is particularly useful for handling structured data such as text files and CSVs. It allows you to perform various operations on text, such as searching for patterns, extracting specific columns, performing calculations, and more.

AWK operates on a per-line basis, processing each line of input sequentially. It uses patterns and actions to determine what to do with each line of input. Patterns are used to match lines, and actions define what to do with the matched lines.

## Directory structure

- **swap_columns**: swap columns in file
  - [link to swap_columns](swap_columns/)

## AWK script structure

AWK scripts typically follow the following structure:

```awk
# This is a comment
BEGIN {
    # Actions to be performed before processing input (optional)
}

{
    # Pattern-action pairs for processing each line of input
    # You can have multiple pattern-action pairs
    # For example:
    # if ($1 > 10) {
    #     print $0;
    # }
}

END {
    # Actions to be performed after processing all input (optional)
}
```

- The **BEGIN** block contains actions to be performed before processing the input. It is optional and typically used for initializing variables or setting up initial conditions.
- The main body of the AWK script processes each line of input based on pattern-action pairs. Patterns can be regular expressions or conditions, and actions define what to do when a line matches the pattern.
- The **END** block contains actions to be performed after processing all input. It is also optional and can be used for final calculations or output.

You can run the AWK script using the `-f` flag to source the program with the script file instead of firsts command line arguments.  
```bash
awk -f script_file.awk
```


## Using AWK from the Command Line

You can also use AWK directly from the command line to perform text processing tasks without creating a separate script. Here's an example of how to use AWK from the command line:

```bash
$ awk '{print $1 }' input.txt
```

- `awk` is the AWK command. 
- `'{print $1}'` is the AWK script provided within single quotes. 
- `input.txt` is the input file that AWK will process. 

Using AWK from the command line can be handy for quick text processing tasks or one-liner operations.


## Built-in Variables in AWK

  - **NR**: NR command keeps a current count of the number of input records. Remember that records are usually lines. Awk command performs the pattern/action statements once for each record in a file. 
  - **NF**: NF command keeps a count of the number of fields within the current input record. 
  - **FS**: FS command contains the field separator character which is used to divide fields on the input line. The default is “white space”, meaning space and tab characters. FS can be reassigned to another character (typically in BEGIN) to change the field separator. 
  - **RS**: RS command stores the current record separator character. Since, by default, an input line is the input record, the default record separator character is a newline. 
  - **OFS**: OFS command stores the output field separator, which separates the fields when Awk prints them. The default is a blank space. Whenever print has several parameters separated with commas, it will print the value of OFS in between each parameter. 
  - **ORS**: ORS command stores the output record separator, which separates the output lines when Awk prints them. The default is a newline character. print automatically outputs the contents of ORS at the end of whatever it is given to print. 

## References

- [AWK command in Unix/Linux with examples - GeeksforGeeks](https://www.geeksforgeeks.org/awk-command-unixlinux-examples/)
