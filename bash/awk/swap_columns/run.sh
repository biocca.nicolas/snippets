#!/bin/bash
#
# Testing awk script
# 

set -e

echo "info: swap columns 1 and 2."
awk -f swap_columns 3 1 2 test_file.txt 

echo "info: swap columns 1 and 2 again, so file remains as it was." 
awk -f swap_columns 3 1 2 test_file.txt 

echo "\n\ninfo: awk can run as is (set exec permissons)"
./swap_columns.awk 3 1 2 test_file.txt 
./swap_columns.awk 3 1 2 test_file.txt 
