#!/usr/bin/awk -f
#--------------------------------------------------------------------
# AWK script to swap two colums in specified section of the document, 
# indicated with initial and final line numbers.
#
# CURRENT BEHAVIOUR:
# prints results on standard ouput (terminal) and rewrites the input
# textfile.
#
# SYNOPSIS 
# swapCol startLine endLine col1 col2 textfile
# 
# startLine          start line number  to start swap
# col1,col2          colums to swap (the order doesn't matter)
#
#
# REMARKS:
# The standard file separator that control the input field separator
# is the whitespace " ". That's fine for GPsolver files, but in order
# to manipulates files like csv (comma-separated values) files it's 
# possible to set the built-in awk variable FS="," or any othar 
# alternative at the BEGIN section. 
# 
# Author: Nicolás Biocca
#--------------------------------------------------------------------
BEGIN {
    if (ARGV[1]=="help" || ARGV[1]=="h")
    {
        print "help message"
        exit 1
    }
    startLine = ARGV[1]
    col1      = ARGV[2]
    col2      = ARGV[3]
    # after BEGIN section, awk it's going to want to all filres
    # specified in ARGV[#], so it's a good practice (a mandatory
    # one) to put them blank.
    ARGV[1]=""
    ARGV[2]=""
    ARGV[3]=""
}

{
    if (FNR>=startLine)
    {
        t     = $col1;
        $col1 = $col2;
        $col2 = t;
        print
    }
    else
    {
        print 
    }
}

# rewrite input file
{
    print > FILENAME
}


