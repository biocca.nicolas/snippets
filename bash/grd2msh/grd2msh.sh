#!/bin/bash

#
# Translate grd (shyfem) mesh to msh (v2 ascii) mesh
#
# Author: Nicolás Biocca

# stop execution on any error
set -e

GRD_FILE=$1
MSH_FILE=$(basename -s .grd $GRD_FILE).msh

echo "info: starting grd2msh script"
echo "input file  = $GRD_FILE"
echo "output file = $MSH_FILE"

#NB_NODES=$(awk '{if ($1 == 1) i++;} END {print i}' $GRD_FILE)
#NB_ELEMS=$(awk '{if ($1 == 2) i++;} END {print i}' $GRD_FILE)
#NB_LINES=$(awk '{if ($1 == 3) i++;} END {print i}' $GRD_FILE)

# deteriotes readability but improves performance
read NB_NODES NB_ELEMS NB_LINES <<< $(awk 'BEGIN {i=0;j=0;k=0} {if ($1==1) i++; if ($1==2) j++; if ($1==3) k++;} END {print i, j, k}' $GRD_FILE)


echo "\$MeshFormat" > $MSH_FILE
echo "2.2 0 8" >> $MSH_FILE
echo "\$EndMeshFormat" >> $MSH_FILE

echo "\$Nodes" >> $MSH_FILE
echo "$NB_NODES" >> $MSH_FILE
awk '{if ($1 == 1) print $2, $4, $5, 0}' $GRD_FILE >> $MSH_FILE
echo "\$EndNodes" >> $MSH_FILE


echo "\$Elements" >> $MSH_FILE
echo "$NB_ELEMS" >> $MSH_FILE
awk '{if ($1 == 2) print $2, "2 4 1 1 1 0", $5, $6, $7}' $GRD_FILE >> $MSH_FILE  
echo "\$EndElements" >> $MSH_FILE

echo "info: done"
