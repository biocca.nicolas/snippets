# Bash Script for Creating Videos from Image Sequences

This Bash script allows you to create video files from a sequence of images using `ffmpeg` from the ImageMagick tools. 
You can specify various options to customize the output video.

## Usage

```shell
Usage: ./image2video.sh [OPTIONS] ...

Options:
  -h   | --help                 Print the usage
  -wxh | --width-x-height       Specify image size (width x height) [pixels]
                                Currently not needed when using pattern-like names.
  -o   | --output-video         Name of the output video file (default = video)
  -i   | --image-files          Name of image files as a pattern (for example: image.%04d.png)
                                Also accepts pattern-like names using wildcards (e.g., *, ?, etc.).
                                Enclose pattern-like names in double quotation marks (e.g., "*.png")
                                Otherwise, chances are Bash will expand the wildcard automatically.
  -fps | --frames-per-second    Frames per second (default = 10)
```

Creates a video file from a sequence of PNG images.
- A pattern like 'image.%04d.png' expects files like image.0000.png, image.0001.png, etc.

## Prerequisites

- This script requires `ffmpeg` from the ImageMagick tools to be installed on your system.

## Examples

Create a video from images with default settings:
```shell
./image2video.sh -i "image*.png"
```

## Installation (WiP)

To install and make the script accessible from anywhere on your system, follow these steps:


Otherwise, we can provide some `install.sh` script or `Makefile`. 


## TODO:

- Support for bash wildcards 
- Add tests 
- Automate installation and testing stages.   
