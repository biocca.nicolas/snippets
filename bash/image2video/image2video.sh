#!/bin/bash
#--------------------------------------------------------------------
# 
# Script to animate a set of images by means of ffmpeg. 
#
# Author: Nicolás Biocca
# Contact: <biocca.nicolas@gmail.com>
#
#--------------------------------------------------------------------

usage() {
    cat <<USAGE
Usage: ${0##*/} [OPTIONS] ...
options:
  -h   | --help                 print the usage
  -wxh | --width-x-height       specify image size (width x height) [pixels]
                                currently no needed when using pattern-like names. 
  -o   | --output-video         name of output video file (default = video)
  -i   | --image-files          name of images files as a patternt (for exameple: image.%04d.png)
                                also accepts pattern-like names using wildcards (e.g., *, ?, and so on)
                                enclose pattern-like names in double quotation marks (e.g., "*.png")
                                otherwise chances are bash will expand the wildcard automatically
  -fps | --frames-per-second    frames per second (default = 10)

Creates a video files from a sequence of PNG images
- A pattern like 'image.%04d.png' will expect files image.0000.png, image.0001.png, etc.

Requires ffmpeg from imagemagick tools 

USAGE
}

error() {
    exec 1>&2
    while [ "$#" -ge 1 ]; do echo "$1"; shift; done
    usage
    exit 1
}


# default settings
frameRate=10

while [ "$#" -gt 0 ]
do
    case $1 in 
        -h | --help ) usage && exit 0;;
        -wxh | --width-x-height ) WxH="$2"; shift 2;;
        -o | --output-video ) outputName="$2"; shift 2;;
        -i | --input-images ) fileName="$2"; shift 2;;
        -fps | --frame-rate ) frameRate="$2"; shift 2;;
        -*) error "invalid option '$1'";;
        * ) break;;
    esac
done


# some hard-coded settings
codec="libx264"
format="mp4"

# fileName pattern    same as C printf function. eg: someName-%04d.png
#                     search for someName-0001.png, someName-0002.png, ...  
# -pattern_type glob: allows wildcards use. 
# -s WxH              resolution

# support for both kinds of fileName styles 
if [[ $fileName == *%* ]]; then
    #ffmpeg -f image2 -framerate ${frameRate} -i ${fileName} -s ${WxH} -c:v ${codec} -profile:v baseline -level 3.0  -pix_fmt yuv420p ${outputName}
    ffmpeg -f image2 -framerate ${frameRate} -i ${fileName} -c:v ${codec} -profile:v baseline -level 3.0  -pix_fmt yuv420p ${outputName}
else
    ffmpeg -f image2 -pattern_type glob -framerate ${frameRate} -i "$fileName" -c:v ${codec} -profile:v baseline -level 3.0  -pix_fmt yuv420p ${outputName}
fi
