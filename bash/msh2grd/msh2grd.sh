#!/bin/bash

#
# Traslate msh (v2 ASCII) mesh to grd (shyfem) mesh format.
#
# Author: Nicolás Biocca

# stop execution on any error
set -e

echo "# grd2msh "

MSH_FILE=$1
if [[ $MSH_FILE != *.msh ]]; then
    echo "error: Not a valid input file extension (msh)"
    echo "       input file = $MSH_FILE"
    exit 1
fi
GRD_FILE=$(basename -s .msh $MSH_FILE).grd

echo "input file  = $MSH_FILE"
echo "output file = $GRD_FILE"


# check mesh format
MSH_FILE_VERSION=$(grep -A 1 "\$MeshFormat" $MSH_FILE | grep -v "\$MeshFormat" | cut -d ' ' -f 1)
if [[ $MSH_FILE_VERSION != 2.2 ]]; then
    echo "error: Not a valid msh file version. Expected v2.2"
    echo "       file version = $MSH_FILE_VERSION"
    exit 1
fi

echo info: processing nodes
#
# tag1 and tag2 are the line number for tags $Nodes and $EndNodes
#
tag1=$(grep -n "\$Nodes" $MSH_FILE | cut -d ':' -f 1)
tag2=$(grep -n "\$EndNodes" $MSH_FILE | cut -d ':' -f 1)

NB_NODES=$(grep -A 1 "\$Nodes" $MSH_FILE | grep -v "\$Nodes")
echo "number of nodes = $NB_NODES"

#echo $tag1, $tag2, $NB_NODES

if [[ $(( tag2-tag1 - 2 )) -ne $NB_NODES ]]; then
    echo "error: <\$Nodes> block wrongly formatted"
    echo "       <\$Nodes> line    = $tag1"
    echo "       <\$EndNodes> line = $tag2"
    echo "       # nodes           = $NB_NODES"
    exit 1
fi

# print out block of nodes
awk "{if (FNR>$((tag1+1)) && FNR<$tag2) print 1, \$1, 0, \$2, \$3}" $MSH_FILE > $GRD_FILE


echo info: processing elements
tag1=$(grep -n "\$Elements" $MSH_FILE | cut -d ':' -f 1)
tag2=$(grep -n "\$EndElements" $MSH_FILE | cut -d ':' -f 1)

NB_ELEMS=$(grep -A 1 "\$Elements" $MSH_FILE | grep -v "\$Elements")
echo "number of elems = $NB_ELEMS"

if [[ $(( tag2-tag1 - 2 )) -ne $NB_ELEMS ]]; then
    echo "error: <\$Elements> block wrongly formatted"
    echo "       <\$Elements> line    = $tag1"
    echo "       <\$EndElements> line = $tag2"
    echo "       # elements           = $NB_ELEMS"
    exit 1
fi
echo '' >> $GRD_FILE
awk "{if (FNR>$((tag1+1)) && FNR<$tag2 && \$2==2) { idx=\$(3)+3 ; print 2, \$1, 0, 3, \$(idx+1), \$(idx+2), \$(idx+3) }}" $MSH_FILE >> $GRD_FILE

echo info: done
